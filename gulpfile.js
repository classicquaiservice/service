var gulp = require('gulp');
var nodemon = require('gulp-nodemon');
var jshint = require('gulp-jshint');
var config = require('./gulp.config')();
var port = config.appPort || 3000;


gulp.task('default', ['help']);

gulp.task('lint', function() {
    return gulp
        .src(config.alljs)
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('dev', function() {
    var isDev = true;
    nodemon({
            script: config.app,
            ext: 'html js',
            env: {
                'PORT': port,
                'NODE_ENV': isDev ? 'development' : 'build',
            },
            tasks: ['lint']
        })
        .on('restart', function() {
            console.log('restarted!');
        });
});
