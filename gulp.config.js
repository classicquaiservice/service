module.exports = function() {
    var config = {
        alljs: [
            './controllers/**/*.js',
            './models/**/*.js',
            './server.js'
        ],
        app: 'server.js',
        appPort: 3000
    };
    return config;
};