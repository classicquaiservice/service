var express = require('express');
var router = express.Router();
var controller = require('./controllers/profileController.js');
var model = require('./models/profileModel.js');
var mongoose = require('mongoose');


/**
 * Routes will be defined Here
 */
router.post('/profile/signUp',controller.SignUp);
router.get('/profile/signIn',controller.SignIn);


module.exports = router;