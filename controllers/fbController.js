var request = require('request');
var Profile = require('../models/profileModel.js');
var jwt = require('jsonwebtoken');


module.exports = function fbLoginController(req, res, next) {

    var path = 'https://graph.facebook.com/me?access_token=' + req.query.token;

    request(path, function(error, response, body) {

        var data = JSON.parse(body);

        if (!error && response && response.statusCode && response.statusCode === 200) {
            Profile.findOne({
                $or: [{
                    facebookId: data.id
                }, {
                    email: data
                }]
            }, function(err, profile) {
                if (err) {
                    return next(err);
                }

                if (!profile) {
                    var newProfile = new Profile({
                        facebookId: data.id,
                        firstName: data.first_name,
                        lastName: data.last_name,
                        email: data.email,
                        gender: data.gender,
                        password: data.id + data.name
                    });
                    newProfile.save(function(err) {
                        {
                            if (err) {
                                return next(err);
                            }
                        }
                    });
                    var token = jwt.sign({
                        Profile: newProfile
                    }, 'secret');

                    newProfile = newProfile.toObject();
                    newProfile.password = undefined;
                    newProfile.token = token;

                    res.json(newProfile);
                }
            });
        }
    });
};
