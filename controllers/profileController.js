var validator = require('validator');
var Profile = require('../models/profileModel.js');
var request = require('request');
var jwt = require('jsonwebtoken');
var helper = require('./helper.js');

//
exports.SignUp = function(req, res, next) {

    if (!req.body.email || !req.body.password) {
        res.status(500);
        return res.send("error");
    }

    if (req.body.email.indexOf(':') > -1 || req.body.password.indexOf(':') > -1)
        return res.send('Email or password should not contain ":" ');

    var model = new Profile({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email.toLowerCase(),
        password: req.body.password,
    });


    model.save(function(err) {
        if (err)
           {
            if (err.code == 11000)
              return res.send('Email Already Exists!');
            
            else 
              return res.send('Internal error');

           }

        res.send("added");

    });
};

//
exports.SignIn = function(req, res, next) {

    var pwd = helper.BasicDecipher(req);    

    Profile.findOne({email: pwd[0]}, function(err, profile) {

     
      if (err) return res.send(err);
      if (!profile) {res.send("no profile found with email")};

      profile.verifyPassword(pwd[1], function(err, isMatch) {

        console.log(isMatch);

            if (err)
                return res.send(err);

            if (!isMatch) 
              return  res.send('Password does not match!');
            
            var token = jwt.sign({ Profile:profile}, 'secret');
             profile = profile.toObject()
             profile.password = undefined;
             profile.token  = token;

            res.json(profile);

        });
    });

};
