exports.BasicDecipher = function(req,res) {

    var Authorization = req.headers.authorization;
    var result = Authorization.split(' ');
    if (result.length !== 2)
        {return res.send('authorization Header is not correct');}
    if (result[0] !== 'Basic')
        {return res.send('authorization Header is not correct');}
    var Basic = new Buffer(result[1], 'base64');
    return (Basic.toString().split(':'));

};
