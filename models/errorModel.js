exports.Error = function(errMsg,errCode){
   errMsg = typeof errMsg !== 'undefined' ? errMsg : 'errmsg not provided';
   errCode = typeof errCode !== 'undefined' ? errCode : 'ErrorCode not provided';
   return {
    'Error!': 
    {       
     'message':errMsg,
     'ErrorCode':errCode,
     }
}
;};
