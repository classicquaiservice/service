var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
var crypto = require('crypto');

/**
 * [ProfileSchema]
 * @type {mongoose}
 */
var ProfileSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true

    },

    dateCreated: {
        type: Date,
        default: Date.now
    },

    gender: String,

    facebookId: {

        type: Number,
        unique: true,
        required: false,
        sparse: true,
        index: true
    },

});


/**
 * Execute before each profile.save() call
 * @param  {[type]}
 * @return {[type]}
 */
ProfileSchema.pre('save', function(callback) {
    var account = this;

    // Break out if the password hasn't changed
    if (!account.isModified('password')) {
        return callback();
    }


    // Password changed so we need to hash it
    bcrypt.genSalt(5, function(err, salt) {
        if (err) {
            return callback(err);
        }

        bcrypt.hash(account.password, salt, null, function(err, hash) {
            if (err) {
                return callback(err);
            }
            account.password = hash;
            callback();
        });
    });
});

/**
 * @param  {[type]}
 * @param  {Function}
 * @return {[type]}
 */
ProfileSchema.methods.verifyPassword = function(password, cb) {
    bcrypt.compare(password, this.password, function(err, isMatch) {
        if (err) {
            return cb(err);
        }
        cb(null, isMatch);
    });
};


module.exports = mongoose.model('Profile', ProfileSchema);
