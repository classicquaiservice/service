var bodyParser = require('body-parser');
var express = require('express'); 
var config = require('./config/config.js');
var route = require ('./routes.js');
var winston = require('winston');
var mongoose = require('mongoose');




module.exports = function () {
  

  mongoose.connect(config.db.URI + '/' + config.db.databaseName);

  //TODO add validation token
  var app = express();

  app.use(bodyParser.json());

  app.use(express.static('public'));

  app.use('/v1',route);

  app.listen(3000, function() {

    console.log('Example app listening on port 3000!');

    process.on('SIGINT', function() {

    mongoose.connection.close(function () {

    console.log('Mongoose disconnected on app termination');

    process.exit(0);

  });

});

});

};

